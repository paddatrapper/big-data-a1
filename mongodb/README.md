# Project Structure

* `reddit.py`: This is the application

To run the system use:

```
pip3 install pymongo
python3 reddit.py
```

This will install the required libraries and run the application and allow
you to interact with the Reddit system. This assumes that python 3 and pip are installed.
