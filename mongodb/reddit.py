###########################################################################
# RedditPy is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# RedditPy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# RedditPy is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RedditPy. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from pymongo import MongoClient

CLIENT = MongoClient()
DB = CLIENT.reddit

def createAccount():
    name = input('Enter the account name or c to cancel: ')
    if name.strip().lower() == 'c':
        return
    is_over_18 = False
    while True:
        is_over_18 = input('Is the user over 18? (y/N): ').strip().lower()
        if is_over_18 == 'y' or is_over_18 == 'yes':
            is_over_18 = True
            break
        elif is_over_18 == 'n' or is_over_18 == 'no' or is_over_18 == '':
            is_over_18 = False
            break
        print('Please enter \'y\' or \'n\'')

    account = { 'account': {
                    'comment_karma': 0,
                    'has_mail': False,
                    'has_mod_mail': False,
                    'has_verified_mail': False,
                    'inbox_count': 1,
                    'is_friend': False,
                    'is_gold': False,
                    'is_mod': False,
                    'link_karma': 0,
                    'modhash': '',
                    'name': name,
                    'over_18': is_over_18,
                }
              }
    accounts = DB.accounts
    if accounts.count_documents({'name': name}) == 0:
        account_id = accounts.insert_one(account).inserted_id
        print('Created user {name} with id {uid}'.format(name=name,
                                                         uid=account_id))
    else:
        print('Username already exists, please choose another one')
        createAccount()

def findAccount():
    account_name = input('Enter the account name (leave blank for all accounts): ').strip()
    accounts = DB.accounts
    if account_name != '':
        results = accounts.find({'account.name': account_name,})
    else:
        results = accounts.find()
    for account in results:
        print(account)

def editAccount():
    while True:
        account_name = input('Enter the account name: ').strip()
        accounts = DB.accounts
        if account_name != '':
            while True:
                result = accounts.find_one({'account.name': account_name,})
                print(result)
                attribute = input('Enter the attribute to edit: ')
                if attribute not in result['account']:
                    print('Attribute not found')
                    continue
                value = input('Enter the new value for {}: '.format(attribute))
                result = accounts.update_one({'account.name': account_name},
                                             {'$set': {
                                                 'account.{}'.format(attribute): value
                                             }})
                print('Modified {:d}'.format(result.modified_count))
                return
        else:
            print('Please enter an account name')

def deleteAccount():
    while True:
        account_name = input('Enter the account name: ').strip()
        accounts = DB.accounts
        if account_name != '':
            result = accounts.delete_one({'account.name': account_name,})
            print('Deleted {:d}'.format(result.deleted_count))
            break;
        else:
            print('Please enter an account name')

if __name__ == '__main__':
    print('Welcome to RedditPy!')
    while True:
        print('Select a menu item number to proceed:')
        print('1) Create account')
        print('2) Find accounts')
        print('3) Edit account')
        print('4) Delete account')
        print('5) Exit')
        selection = input('Select a option: ').strip()
        if selection == '1':
            createAccount()
        elif selection == '2':
            findAccount()
        elif selection == '3':
            editAccount()
        elif selection == '4':
            deleteAccount()
        elif selection == '5':
            break
        else:
            print('Input not recognized, please select an available option')
        print()
