/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

/**
 *
 * @author dube_
 */
public class Db_assignment {

    private static CouchDbConnector db;
    private static CouchDbInstance dbInstance;
    private static HttpClient httpClient;

    public static List<Double> random_list() {
	List<Double> list = new ArrayList<>();
	Random rand = new Random();
	for (int i = 0; i < 10; ++i) {
	    list.add(rand.nextDouble() * 100);
	}
	return list;
    }

    public static void create(String name, String std_number, String email, String country, String phone_number) {
	Map<String, Object> referenceData = new HashMap<>();
	Student s = new Student(name, email, phone_number, country, std_number, random_list());
	referenceData.put("_id", s.getStudent_number());
	referenceData.put("Data", s);
	db.create(referenceData);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException {
	// TODO code application logic here
	//--------------- Creating Connection--------------------------//  
	httpClient = new StdHttpClient.Builder().url("http://localhost:5984")
		.username("admin")
		.password("admin")
		.build();
	dbInstance = new StdCouchDbInstance(httpClient);
	//--------------- Creating database----------------------------//  
	db = new StdCouchDbConnector("students", dbInstance);

	db.createDatabaseIfNotExists();
	//--------------- Creating Document----------------------------//  
	create("Meluleki Dube", "DBXMEL004", "DBXMEL004@myuct.ac.za", "Zimbabwean", "061285475");
	create("David Kheri", "KHRDAV001", "KHRDAV001@myuct.ac.za", "Tanzania", "0612365968");
	create("Clayton Sibanda", "SBNCLA002", "SBNCLA002@myuct.ac.za", "South Africa", "0636874520");
	
	// Get opperation//
	Map<String, Object> result = db.get(Map.class, "DBXMEL004");
	System.out.println((result.get("Data")));
	//Update//
	String phone = "0717782245";
	((Map)result.get("Data")).put("phone_number", phone);
	db.update(result);
	System.out.println((db.get(Map.class, "DBXMEL004").get("Data")));
	//Delete//
	create("Kyle Robbertze", "RBBKYL001", "RBBKYL001@myuct.ac.za", "Nigeria", "067896625");
	result =db.get(Map.class, "RBBKYL001");
	System.out.println(result.get("Data"));
	db.delete(result);
	try{
	    result =db.get(Map.class, "RBBKYL001");
	    System.out.println(result); 
	}catch(Exception e){
	    System.out.println(e);
	}
	
    }
}
