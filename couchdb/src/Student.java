/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author dube_
 */
public class Student implements Serializable{
     private static final long serialVersionUID = 727813829995624926L;
     private String student_number, name, email, phone_number, nationality;

    public String getStudent_number() {
	return student_number;
    }

    public void setStudent_number(String student_number) {
	this.student_number = student_number;
    }
     private List<Double> marks;

    public Student(String name, String email, String phone_number, String nationality, String student_number, List<Double> marks) {
	this.name = name;
	this.email = email;
	this.phone_number = phone_number;
	this.nationality = nationality;
	this.student_number = student_number;
	this.marks = marks;
    }

    public static long getSerialVersionUID() {
	return serialVersionUID;
    }

    public String getName() {
	return name;
    }

    public String getEmail() {
	return email;
    }

    public String getPhone_number() {
	return phone_number;
    }

    public String getNationality() {
	return nationality;
    }

    public List<Double> getMarks() {
	return marks;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setPhone_number(String phone_number) {
	this.phone_number = phone_number;
    }

    public void setNationality(String nationality) {
	this.nationality = nationality;
    }

    public void setMarks(List<Double> marks) {
	this.marks = marks;
    }

    @Override
    public String toString() {
	return student_number+" : "+name+" "+Arrays.toString(marks.toArray());
    }

    @Override
    public boolean equals(Object obj) {
	if(obj == null)
	    return false;
	if(!(obj instanceof Student))
	    return false;
	Student temp = (Student) obj;
	return this.getStudent_number().equals(temp.getStudent_number());
    }
     
   
}
