# Project Structure

* `src`: This contains the java files that are have the functionality
* `lib`: Contains the libraries needed for 
* `Makefile`: for building and running the files

To run the system use: 

```
make run
```

This will compile and run the application. And print the results as each queuery
is being run.

Requirements for the application are that you need to have Java and couchdb installed. Your couchDB instance must also be running.
